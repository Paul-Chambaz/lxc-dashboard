#include "title.h"

void
title_window_print ( lxc_data data, window win )
{
  wclear(win.win);
  if (win.focus) {
    wattron(win.win, A_BOLD);
    wattron(win.win, COLOR_PAIR(3));
  }
  box(win.win, 0, 0);
  wattron(win.win, A_BOLD);
  mvwprintw(win.win, 0, 2, " %s ", win.name);
  wattroff(win.win, A_BOLD);
  if (win.focus) {
    wattroff(win.win, COLOR_PAIR(3));
  }

  print_window_line(win, 0, LEFT, "%s", "lxc-dashboard");
  
  print_window_line(win, 0, RIGHT, "v%s", data.version);

  wrefresh(win.win);
}
