#include "consumption.h"

void
consumption_window_print ( lxc_data data, window win )
{
  wclear(win.win);
  if (win.focus) {
    wattron(win.win, A_BOLD);
    wattron(win.win, COLOR_PAIR(3));
  }
  box(win.win, 0, 0);
  wattron(win.win, A_BOLD);
  mvwprintw(win.win, 0, 2, " %s ", win.name);
  wattroff(win.win, A_BOLD);
  if (win.focus) {
    wattroff(win.win, COLOR_PAIR(3));
  }

  print_window_line(win, 0, LEFT, "%s", "Total cpu usage:");

  print_window_line(win, 1, LEFT, "%s", "Total memory usage:");

  print_window_line(win, 2, LEFT, "%s", "Total swap usage:");

  print_window_line(win, 3, LEFT, "%s", "Total tasks:");

  print_window_line(win, 4, LEFT, "%s", "Total load average:");

  // on this window we want to display the combined usage of all containers
  //
  // that means we want to display:
  // memory usage
  // cpu usage
  // swap usage
  // number of tasks
  // load average

  wrefresh(win.win);
}
