#include "ui.h"

void
to_lower_case ( char *string )
{
  for (int i = 0; i < strlen(string); i++) {
    string[i] = string[i] + 'a' - 'A';
  }
}

void
to_higher_case ( char *string )
{
  for (int i = 0; i < strlen(string); i++) {
    string[i] = string[i] + 'A' - 'a';
  }
}

void
quick_strcpy ( char **dest, const char *src )
{
  *dest = (char *) calloc(strlen(src), sizeof(char));
  strcpy(*dest, src);
}

void
init_window ( window *win, int h, int w, int y, int x, const char *name )
{
  win->h = h;
  win->w = w;
  win->y = y;
  win->x = x;
  win->win = newwin(h, w, y, x);
  win->name = name;
  win->hidden = false;
  win->focus = false;
  win->scroll = 0;
  win->cursor = -1;
}

void
print_window_line ( window win, int y, ui_align align, const char *format, ... )
{
  char buffer[256];
  int x;
  va_list args;

  y++;

  va_start(args, format);
  vsnprintf(buffer, 255, format, args);
  va_end(args);

  if (align == LEFT) {
    x = 1;
  } else if (align == CENTER) {
    x = (win.w - 2 - strlen(buffer)) / 2;
  } else if (align == RIGHT) {
    x = win.w - strlen(buffer) - 1;
  }

  mvwprintw(win.win, y, x, "%.*s", win.w - 2, buffer);

  if (win.focus && win.cursor + 1 == y) {
    mvwchgat(win.win, y, 1, win.w - 2, 0, 2, NULL);
  }
}

void
print_window_line_scroll ( window win, int y, ui_align align, int start, int end, const char *format, ... )
{
  char buffer[256];
  int x;
  va_list args;

  y++;

  if (y - win.scroll < start + 1 || y - win.scroll >= end + 1) {
    return;
  }

  va_start(args, format);
  vsnprintf(buffer, 255, format, args);
  va_end(args);
  
  if (align == LEFT) {
    x = 1;
  } else if (align == CENTER) {
    x = (win.w - 2 - strlen(buffer)) / 2;
  } else if (align == RIGHT) {
    x = win.w - strlen(buffer) - 1;
  }

  mvwprintw(win.win, y - win.scroll, x, "%.*s", win.w - 2, buffer);

  if (win.focus && win.cursor + 1 == y) {
    mvwchgat(win.win, y, 1, win.w - 2, 0, 2, NULL);
  }
}
