#include "containers.h"


void
containers_window_print ( lxc_data data, window win )
{
  wclear(win.win);
  if (win.focus) {
    wattron(win.win, A_BOLD);
    wattron(win.win, COLOR_PAIR(3));
  }
  box(win.win, 0, 0);
  wattron(win.win, A_BOLD);
  mvwprintw(win.win, 0, 2, " %s ", win.name);
  wattroff(win.win, A_BOLD);
  if (win.focus) {
    wattroff(win.win, COLOR_PAIR(3));
  }

  int height = (LINES - 3) / 3 - 2;
  int nb_lines = MIN(height, data.number_containers);
  for (int i = 0; i < nb_lines; i++) {
    print_window_line_scroll(win, i, LEFT, 0, win.h - 2, "%s", data.names[i]);

    char *state;
    quick_strcpy(&state, data.containers[i]->state(data.containers[i]));

    bool is_running = !strcmp(state, "RUNNING");

    if (is_running) {
      wattron(win.win, COLOR_PAIR(1));
      wattron(win.win, A_BOLD);
    }

    to_lower_case(state);

    print_window_line_scroll(win, i, RIGHT, 0, win.h - 2, " %s", state);

    if (is_running) {
      wattroff(win.win, A_BOLD);
      wattroff(win.win, COLOR_PAIR(1));
    }

    free(state);
  }
  wrefresh(win.win);
}
