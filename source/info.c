#include "info.h"

void
info_window_print ( lxc_data data, window win )
{
  int current_container_id = 0;

  wclear(win.win);
  if (win.focus) {
    wattron(win.win, A_BOLD);
    wattron(win.win, COLOR_PAIR(3));
  }
  box(win.win, 0, 0);
  wattron(win.win, A_BOLD);
  mvwprintw(win.win, 0, 2, " %s ", win.name);
  wattroff(win.win, A_BOLD);
  if (win.focus) {
    wattroff(win.win, COLOR_PAIR(3));
  }

  // on this window we want to display information about the current container - this would be where we print the consumption of the individual container

  if (data.number_containers == 0) {
    wrefresh(win.win);
    return;
  }

  print_window_line(win, 0, CENTER, "[%s]", data.names[current_container_id]);

  print_window_line_scroll(win, 1, LEFT, 1, win.h - 2, "%s", "Cpu usage:");
  print_window_line_scroll(win, 2, LEFT, 1, win.h - 2, "%s", "Memory usage:");
  print_window_line_scroll(win, 3, LEFT, 1, win.h - 2, "%s", "Swap usage:");
  print_window_line_scroll(win, 4, LEFT, 1, win.h - 2, "%s", "Tasks:");
  print_window_line_scroll(win, 5, LEFT, 1, win.h - 2, "%s", "Load average:");
  print_window_line_scroll(win, 6, LEFT, 1, win.h - 2, "%s", "Pid::");
  print_window_line_scroll(win, 7, LEFT, 1, win.h - 2, "%s", "Ip adress:");

  wrefresh(win.win);
}
