#include "struct.h"
#include "title.h"
#include "containers.h"
#include "consumption.h"
#include "info.h"

#define ctrl(x) ((x) & 0x1f)

void down_window ( windows_group *windows );
void up_window ( windows_group *windows );

void init_windows ( windows_group *windows );
void print_windows ( lxc_data data, windows_group windows );
void free_data ( lxc_data *data );

int
main ( )
{
  windows_group windows;
  init_windows(&windows);

  windows.containers_window.focus = true;

  lxc_data data;
  int key;
  while (true) {
    key = getch();
    if (key == 'Q')
      break;

    switch (key) {
      case ctrl('j'):
        down_window(&windows);
        break;
      case ctrl('k'):
        up_window(&windows);
        break;
    }

    quick_strcpy(&data.version, lxc_get_version());
    data.number_containers = list_all_containers(lxc_path, &data.names, &data.containers);

    if (data.number_containers > 0) {
      windows.containers_window.cursor = 0;
      windows.info_window.cursor = 1;
    }

    print_windows(data, windows);

    free_data(&data);
  }

  endwin();
}

void
down_window ( windows_group *windows )
{
  if (windows->title_window.focus) {
    windows->title_window.focus = false;
    windows->containers_window.focus = true;
  } else if (windows->containers_window.focus) {
    windows->containers_window.focus = false;
    windows->consumption_window.focus = true;
  } else if (windows->consumption_window.focus) {
    windows->consumption_window.focus = false;
    windows->info_window.focus = true;
  }
}

void
up_window ( windows_group *windows )
{
  if (windows->containers_window.focus) {
    windows->containers_window.focus = false;
    windows->title_window.focus = true;
  } else if (windows->consumption_window.focus) {
    windows->consumption_window.focus = false;
    windows->containers_window.focus = true;
  } else if (windows->info_window.focus) {
    windows->info_window.focus = false;
    windows->consumption_window.focus = true;
  }
}

void
free_data ( lxc_data *data )
{
  free(data->version);
  free(data->names);
  free(data->containers);
}

void
init_windows ( windows_group *windows )
{
  initscr();
  keypad(stdscr, TRUE);
  timeout(16);
  refresh();
  raw();
  noecho();
  curs_set(0);

  start_color();
  init_pair(1, 2, 0);
  init_pair(2, 0, 12);
  init_pair(3, 1, 0);

  init_window(&windows->title_window, 3, side_width, 0, 0, title_title_str);

  init_window(&windows->containers_window, (LINES - 3) / 3, side_width, 3, 0, containers_title_str);

  init_window(&windows->consumption_window, 7, side_width, (LINES - 3) / 3 + 3, 0, consumption_title_str);

  init_window(&windows->info_window, LINES - (LINES - 3) / 3 - 3 - 7, side_width, (LINES - 3) / 3 + 3 + 7, 0, info_title_str);
}

void
print_windows ( lxc_data data, windows_group windows )
{
  title_window_print(data, windows.title_window);
  containers_window_print(data, windows.containers_window);
  consumption_window_print(data, windows.consumption_window);
  info_window_print(data, windows.info_window);
}
