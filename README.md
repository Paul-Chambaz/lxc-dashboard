PROJECTNAME
=

About
-

Installation
-

```
git clone https://git.chambaz.xyz/lxc-dashboard
cd lxc-dashboard/build
sudo make clean install
```

Licence
-

This project is licenced under the GPLv2 licence.
For more information, read the LICENSE file.
