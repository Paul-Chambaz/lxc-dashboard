#ifndef CONTAINERS_H
#define CONTAINERS_H

#include "struct.h"
#include "ui.h"

static const char *containers_title_str = "Containers";

void containers_window_print ( lxc_data data, window window );

#endif
