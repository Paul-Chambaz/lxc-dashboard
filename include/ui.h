#ifndef UI_H
#define UI_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "struct.h"

void to_lower_case ( char *string );
void to_higher_case ( char *string );
void quick_strcpy ( char **dest, const char *src );

void init_window ( window *win, int h, int w, int y, int x, const char *name );

void print_window_line ( window win, int y, ui_align align, const char *format, ... );
void print_window_line_scroll ( window win, int y, ui_align align, int start, int end, const char *format, ... );

#endif
