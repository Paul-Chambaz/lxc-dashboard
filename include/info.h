#ifndef INFO_H
#define INFO_H

#include "struct.h"
#include "ui.h"

static const char *info_title_str = "Info";

void info_window_print ( lxc_data data, window win );

#endif
