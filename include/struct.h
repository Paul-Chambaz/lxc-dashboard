#ifndef STRUCT_H
#define STRUCT_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <lxc/lxccontainer.h>
#include <curses.h>

#define MIN(x, y) (x > y) ? y : x

typedef enum {
  LEFT,
  CENTER,
  RIGHT,
} ui_align;

typedef struct {
  char *version;
  int number_containers;
  char **names;
  struct lxc_container **containers;
} lxc_data;

typedef struct {
  int x;
  int y;
  int w;
  int h;
  WINDOW *win;
  const char *name;
  bool hidden;
  bool focus;
  int scroll;
  int cursor;
} window;

typedef struct {
  window title_window;
  window containers_window;
  window consumption_window;
  window info_window;
  window log_window;
  window stat_window;
  window services_window;
  window process_window;
  window config_window;
} windows_group;

static const char *lxc_path = "/var/lib/lxc";

static const int side_width = 36;

#endif
