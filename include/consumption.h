#ifndef CONSUMPTION_H
#define CONSUMPTION_H

#include "struct.h"
#include "ui.h"

static const char *consumption_title_str = "Usage";

void consumption_window_print ( lxc_data data, window win );

#endif
